/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backend;

import java.util.List;
import persistence.Pet;
import persistence.PetController;

/**
 *
 * @author jorge
 */
public class Logic {
    private String [] alergico = {"Al agua", "A las personas", "A los gatos", "A la morfinas"};
    private String [] atencion = {"VIP", "Basic", "Medium"};
    private Pet pet = null;
    private PetController petController;

    public Logic() {
        petController = new PetController();
    }

    public String[] getAlergico() {
        return alergico;
    }

    public String[] getAtencion() {
        return atencion;
    }

    public Pet savePet(String name_pet, String raza_pet, String color_pet, 
            String alergico_pet, String attention_pet, String name_owner_pet, 
            String phone_owner_pet, String observation_pet){
        int phone = 0;
        try{
            phone = Integer.parseInt(phone_owner_pet);
        } catch (NumberFormatException e){
            System.out.println(phone_owner_pet + " is not a number\n" + e.toString());
        }
        pet = new Pet(name_pet, raza_pet, color_pet, alergico_pet, attention_pet, name_owner_pet, 
                phone, observation_pet);
        pet = petController.createPets(pet);
        return pet;
    }
    
    public List<Pet> getAllPets(){
        return petController.getAllPets();
    }
}
