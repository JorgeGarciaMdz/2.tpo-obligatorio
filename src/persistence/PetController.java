/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.util.List;

/**
 *
 * @author jorge
 */
public class PetController {
    private PetJpaController petJpaController;
    
    public PetController(){
        petJpaController = new PetJpaController();
    }
    
    public void createPet(Pet pet){
        try{
            petJpaController.create(pet);
        } catch(Exception ex){
            System.out.println("Error: " + ex.toString());        }
    }
    
    public Pet createPets(Pet pet){
        try{
            return petJpaController.createPet(pet);
        } catch (Exception ex){
            System.out.println("Error: " + ex.toString());
        }
        return null;
    }
    
    public List<Pet> getAllPets(){
        return petJpaController.findPetEntities();
    }
}
