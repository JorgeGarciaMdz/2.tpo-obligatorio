/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jorge
 */
@Entity
@Table(name="pets")
public class Pet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id")
    private Long id_pet;

    @Basic
    @Column(name="name")
    private String name_pet;
    @Column(name="raza")
    private String raza_pet;
    @Column(name="color")
    private String color_pet;
    @Column(name="alergico")
    private String alergico_pet;
    @Column(name="attention")
    private String attention_pet;
    @Column(name="name_owner")
    private String name_owner_pet;
    @Column(name="phone_owner")
    private int phone_owner_pet;
    @Column(name="observation")
    private String observation_pet;
    
    public Pet() {
    }

    public Pet(String name_pet, String raza_pet, String color_pet, String alergico_pet, String attention_pet, String name_owner_pet, int phone_owner_pet, String observation_pet) {
        this.name_pet = name_pet;
        this.raza_pet = raza_pet;
        this.color_pet = color_pet;
        this.alergico_pet = alergico_pet;
        this.attention_pet = attention_pet;
        this.name_owner_pet = name_owner_pet;
        this.phone_owner_pet = phone_owner_pet;
        this.observation_pet = observation_pet;
    }

    public Pet(Long id_pet, String name_pet, String raza_pet, String color_pet, String alergico_pet, String attention_pet, String name_owner_pet, int phone_owner_pet, String observation_pet) {
        this.id_pet = id_pet;
        this.name_pet = name_pet;
        this.raza_pet = raza_pet;
        this.color_pet = color_pet;
        this.alergico_pet = alergico_pet;
        this.attention_pet = attention_pet;
        this.name_owner_pet = name_owner_pet;
        this.phone_owner_pet = phone_owner_pet;
        this.observation_pet = observation_pet;
    }

    public Long getId_pet() {
        return id_pet;
    }

    public void setId_pet(Long id_pet) {
        this.id_pet = id_pet;
    }

    public String getName_pet() {
        return name_pet;
    }

    public void setName_pet(String name_pet) {
        this.name_pet = name_pet;
    }

    public String getRaza_pet() {
        return raza_pet;
    }

    public void setRaza_pet(String raza_pet) {
        this.raza_pet = raza_pet;
    }

    public String getColor_pet() {
        return color_pet;
    }

    public void setColor_pet(String color_pet) {
        this.color_pet = color_pet;
    }

    public String getAlergico_pet() {
        return alergico_pet;
    }

    public void setAlergico_pet(String alergico_pet) {
        this.alergico_pet = alergico_pet;
    }

    public String getAttention_pet() {
        return attention_pet;
    }

    public void setAttention_pet(String attention_pet) {
        this.attention_pet = attention_pet;
    }

    public String getName_owner_pet() {
        return name_owner_pet;
    }

    public void setName_owner_pet(String name_owner_pet) {
        this.name_owner_pet = name_owner_pet;
    }

    public int getPhone_owner_pet() {
        return phone_owner_pet;
    }

    public void setPhone_owner_pet(int phone_owner_pet) {
        this.phone_owner_pet = phone_owner_pet;
    }

    public String getObservation_pet() {
        return observation_pet;
    }

    public void setObservation_pet(String observation_pet) {
        this.observation_pet = observation_pet;
    }

    
 
    

    public String toString() {
        return "id: " + id_pet + "\n"
                + "name:" + name_pet + "\n"
                + "raza: " + raza_pet + " \n"
                + "color: " + color_pet + "\n"
                + "property: " + name_owner_pet + "\n"
                + "phoner property: " + phone_owner_pet + "\n"
                + "Observation: " + observation_pet;
    }

}
